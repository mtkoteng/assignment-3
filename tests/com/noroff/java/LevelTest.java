package com.noroff.java;

import org.junit.Test;

import static org.junit.jupiter.api.Assertions.*;

class LevelTest {
    Level level1 = new Level();
    Level level2 = new Level();
    Level level3 = new Level();
    Level level4 = new Level();

    @org.junit.jupiter.api.Test
    void getCurrentLevel() {
        assertEquals(1, level1.getCurrentLevel(99));
        assertEquals(2, level2.getCurrentLevel(109));
        assertEquals(4, level3.getCurrentLevel(122));
        assertEquals(5, level4.getCurrentLevel(145));
    }
    @org.junit.jupiter.api.Test
    void getXpToNextLevel(){
        assertEquals(1, level2.xpToNextLevel(109));
        assertEquals(11, level3.xpToNextLevel(122));
        assertEquals(1, level4.xpToNextLevel(145));
    }
}