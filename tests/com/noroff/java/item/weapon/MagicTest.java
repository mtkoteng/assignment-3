package com.noroff.java.item.weapon;

import com.noroff.java.Level;
import com.noroff.java.hero.Hero;
import com.noroff.java.hero.HeroType;
import com.noroff.java.hero.Mage;
import com.noroff.java.hero.Warrior;
import com.noroff.java.item.Item;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MagicTest {

    Weapon magic1 = new Magic(1, "Magico");
    Weapon magic2 = new Magic(2, "Magico");

    @Test
    void getBaseDamage() {
        assertEquals(27, magic1.getBaseDamage());
        assertEquals(29, magic2.getBaseDamage());
    }


}