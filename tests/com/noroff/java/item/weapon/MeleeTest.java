package com.noroff.java.item.weapon;

import com.noroff.java.Level;
import com.noroff.java.hero.*;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MeleeTest {

    Melee melee1 = new Melee(3, "Gun");
    Melee melee2 = new Melee(2, "Great Axe");
    @Test
    void getBaseDamage() {

        assertEquals(21, melee1.getBaseDamage());
        assertEquals(19, melee2.getBaseDamage());
    }
}