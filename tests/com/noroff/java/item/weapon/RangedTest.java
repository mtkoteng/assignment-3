package com.noroff.java.item.weapon;

import com.noroff.java.hero.*;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RangedTest {
    Ranged ranged1 = new Ranged(2,"Rappapapapaphh");
    Ranged ranged2 = new Ranged(1, "Ra ra rappah");
    @Test
    void getBaseDamage() {
        assertEquals(11, ranged1.getBaseDamage());
        assertEquals(8, ranged2.getBaseDamage());
    }

}