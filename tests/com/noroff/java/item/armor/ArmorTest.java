package com.noroff.java.item.armor;

import com.noroff.java.item.Slot;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ArmorTest {
    Armor armor = new Cloth(1, "Helmet",  Slot.HEAD);
    Armor armor2 = new Leather(1, "West",  Slot.BODY);
    Armor armor3 = new Plate(2, "Shoes",  Slot.LEGS);

    @Test
    void findScale() {
        assertEquals(12, armor.bonusHp);
        assertEquals(28, armor2.bonusHp);
        assertEquals(32, armor3.bonusHp);

        assertEquals(4, armor.bonusInt);
        assertEquals(2, armor2.bonusInt);
        assertEquals(0, armor3.bonusInt);

    }
}