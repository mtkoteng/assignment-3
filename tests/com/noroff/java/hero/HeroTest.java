package com.noroff.java.hero;

import com.noroff.java.Level;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class HeroTest {
    Hero hero1 = new Hero(HeroType.WARRIOR, new Warrior());
    Hero hero2 = new Hero(HeroType.MAGE, new Mage());
    Hero hero3 = new Hero(HeroType.RANGER, new Ranger());


    @Test
    void getBaseStats() {
        assertEquals(150, hero1.baseStats.health);
        assertEquals(100, hero2.baseStats.health);
        assertEquals(120, hero3.baseStats.health);
        assertEquals(1, hero1.baseStats.intelligence);
        assertEquals(10, hero2.baseStats.intelligence);
        assertEquals(2, hero3.baseStats.intelligence);

    }

    @Test
    void getCurrentLevel() {
        hero1.setXp(120); //level 3 warrior
        hero2.setXp(101);
        hero3.setXp(130);
        assertEquals(210, hero1.baseStats.health);
        assertEquals(115, hero2.baseStats.health);
        assertEquals(180, hero3.baseStats.health);

    }

}