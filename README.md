# Assignment 3 - RPG characters
## Run
The program can be runned from out/artifacts and then run the jar-file.

All tests can be runned from the test folder.

## The program
This program is handling the creation of some characters and giving them the possibillity to collect some items. These items updates the scores of the hero depending on what type of armor it is. The application also allows the heroes to attack with weapons. The program prints out the information, and it is not possible to do anything self.

## Code hierarchy
The implementation of this program is based on the Strategy pattern and inheritence. For the part with the heroes, the Strategy pattern is used. For Items, inheritence is used. The heroes is getting their stats calculated based on what type they are, and what level they are in. The limit of the levels is calculated in the Level class. 
For the Items, we have a double inheritence. First Weapon and Armor is inheriting the Items class. Then Magic, Melee and Ranged is inheriting the Weapon class, and Cloth, Leather and Plate is inheriting the Armor class. These classes are handling the bonuses when a hero is collecting the different items.
![alt text](src/Diagram/Untitled Diagram.png "Title Text")

## Tests
All tests are junit tests, which see if the methods are giving the expected output.
