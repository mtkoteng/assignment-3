package com.noroff.java;

import com.noroff.java.hero.*;
import com.noroff.java.item.Slot;
import com.noroff.java.item.armor.Armor;
import com.noroff.java.item.armor.Cloth;
import com.noroff.java.item.armor.Leather;
import com.noroff.java.item.armor.Plate;
import com.noroff.java.item.weapon.Magic;
import com.noroff.java.item.weapon.Melee;
import com.noroff.java.item.weapon.Ranged;

public class Main {

    public static void main(String[] args) {
        Hero mage = new Hero(HeroType.MAGE, new Mage());
        Hero ranger = new Hero(HeroType.RANGER, new Ranger());
        Hero warrior = new Hero(HeroType.WARRIOR, new Warrior());
        Cloth cloth = new Cloth(3, "Shoes", Slot.LEGS);
        Leather leather = new Leather(2, "Helmet", Slot.HEAD);
        Plate plate = new Plate(1, "West", Slot.BODY);
        Magic  magic = new Magic(1, "Magee");
        Melee melee = new Melee(5, "Melee");
        Ranged ranged = new Ranged(10, "Ranged");

        System.out.println("Creating Mage character: ");
        mage.setXp(101);
        System.out.println(mage.print());
        System.out.println("-------------------------------------");

        System.out.println("Creating Ranger character: ");
        ranger.setXp(125);
        System.out.println(ranger.print());
        System.out.println("-------------------------------------");

        System.out.println("Creating warrior character: ");
        warrior.setXp(144);
        System.out.println(warrior.print());
        System.out.println("-------------------------------------");

        System.out.println("Creating a Melee weapon:");
        System.out.println(melee.printInfo());
        System.out.println("-------------------------------------");
        System.out.println("Creating a Magic weapon:");
        System.out.println(magic.printInfo());
        System.out.println("-------------------------------------");
        System.out.println("Creating a Ranges weapon:");
        System.out.println(ranged.printInfo());
        System.out.println("-------------------------------------");

        System.out.println("Creating a Cloth armor:");
        System.out.println(cloth.printInfo());
        System.out.println("-------------------------------------");
        System.out.println("Creating a Leather armor:");
        System.out.println(leather.printInfo());
        System.out.println("-------------------------------------");
        System.out.println("Creating a Plate armor:");
        System.out.println(plate.printInfo());
        System.out.println("-------------------------------------");

        //Adding items
        System.out.println("Adding ranged weapon, cloth and leather armor to warrior:");
        System.out.println("Before: \n" + warrior.print());
        warrior.addItem(ranged);
        warrior.addItem(cloth);
        warrior.addItem(leather);
        System.out.println("After: \n" + warrior.print());
        System.out.println("-------------------------------------");


        System.out.println("Adding Melee weapon and Plate to Ranger:");
        System.out.println("Before: \n" + ranger.print());          //lvl: 4
        ranger.addItem(melee);
        ranger.addItem(plate);
        System.out.println("After: \n" + ranger.print());
        System.out.println("-------------------------------------");

        System.out.println("Removes Cloth from warrior, and adds a new one");
        warrior.removeItem(cloth);
        Armor newCloth = new Cloth(2, "New west", Slot.BODY);
        warrior.addItem(newCloth);
        System.out.println(warrior.print());
        System.out.println("-------------------------------------");
        System.out.println("Tries to add an armor with to high level");
        Armor newArmor = new Leather(8, "New west", Slot.HEAD);
        warrior.addItem(newArmor);
        System.out.println(warrior.print());

        System.out.println("-------------------------------------");
        System.out.println("Warrior attacking with damage: " + warrior.getTotalDamage());
        System.out.println("Ranger attacking with damage: " + ranger.getTotalDamage());

    }
}
