package com.noroff.java.hero;

import com.noroff.java.Level;
import com.noroff.java.item.Item;
import com.noroff.java.item.Slot;
import com.noroff.java.item.armor.Armor;
import com.noroff.java.item.weapon.Magic;
import com.noroff.java.item.weapon.Melee;
import com.noroff.java.item.weapon.Ranged;
import com.noroff.java.item.weapon.Weapon;

import java.util.HashMap;
import java.util.Map;

/*
* The Hero class is implemented using the Strategy pattern.
* It handles the calculations of the stats of the Heroes, and aligns items to them.
* It also handles the stats updates when the hero is levelling up.
*/

public class Hero {
    protected int xp;
    protected Level level;
    HeroType type;
    HeroAttribute baseStats;
    HeroAttributeStrategy attributeStrategy;

    private HashMap<Slot, Item> items = new HashMap<>();

    public Hero(HeroType type, HeroAttributeStrategy attributeStrategy){
        this.type = type;
        this.attributeStrategy = attributeStrategy;
        this.baseStats = new HeroAttribute();
        this.level = new Level();
        getBase();
    }

    private void getBase(){
        attributeStrategy.getBaseStats(baseStats);
    }
    public void levelUp(){
        attributeStrategy.onLevelUp(baseStats);
    }

    public HeroAttribute getBaseStats(){
        return baseStats;
    }

    public HeroType getType() {
        return type;
    }

    public int getXp() {
        return xp;
    }

    public void setXp(int xp) {
        this.xp = xp;
        calculateStats();
    }

    public Level getLevel() {
        return level;
    }

    public void setLevel(Level level) {
        this.level = level;
    }

    public int getCurrentLevel(){
        return level.getCurrentLevel(xp);
    }

    public int xpToNextLevel(){ return level.xpToNextLevel(xp); }

    public void calculateStats(){
        for (int i = 1; i < getCurrentLevel(); i++) {
            levelUp();
        }
    }

    //Adds Item to a slot. If it is an item of the specific slot, the new item replaces the older.
    public void addItem(Item item){
        if (getCurrentLevel() >= item.getNeededLevel()){
            items.put(item.getSlot(), item);
        }
    }
    //Removes the item
    public void removeItem(Item item){
        items.remove(item.getSlot());
    }

    //The underlaying methods is calculating the total stats with items added.
    public int getTotalHp(){
        int total = baseStats.health;
        for (Map.Entry<Slot, Item> entry : items.entrySet()){
            if (!(entry.getKey().equals(Slot.WEAPON))){
                total += ((Armor) entry.getValue()).getBonusHp();
            }
        }
        return total;
    }
    public int getTotalStrength(){
        int total = baseStats.strength;
        for (Map.Entry<Slot, Item> entry : items.entrySet()){
            if (!(entry.getKey().equals(Slot.WEAPON))){
                total += ((Armor) entry.getValue()).getBonusStr();
            }
        }
        return total;
    }
    public int getTotalDexterity(){
        int total = baseStats.dexterity;
        for (Map.Entry<Slot, Item> entry : items.entrySet()){
            if (!(entry.getKey().equals(Slot.WEAPON))){
                total += ((Armor) entry.getValue()).getBonusDex();
            }
        }
        return total;
    }
    public int getTotalIntelligence(){
        int total = baseStats.intelligence;
        for (Map.Entry<Slot, Item> entry : items.entrySet()){
            if (!(entry.getKey().equals(Slot.WEAPON))){
                total += ((Armor) entry.getValue()).getBonusInt();
            }
        }
        return total;
    }

    public int getTotalDamage(){
        if (!(items.containsKey(Slot.WEAPON))){
            return 0;
        }
        Weapon weapon = (Weapon) items.get(Slot.WEAPON);
        int baseDamage = weapon.getBaseDamage();
        if (weapon instanceof Magic){
            baseDamage += getTotalIntelligence() * 3;
        }
        if (weapon instanceof Melee){
            baseDamage += getTotalStrength() * 1.5;
        }
        if (weapon instanceof Ranged){
            baseDamage += getTotalDexterity() * 2;
        }
        return baseDamage;
    }

    public String print(){
        String res = "";
        if (type == HeroType.MAGE){
            res += "Mage details:\n";
        }
        if (type == HeroType.RANGER){
            res += "Ranger details:\n";
        }
        if (type == HeroType.WARRIOR){
            res += "Warrior details:\n";
        }
        res += "HP: " + getTotalHp() + "\nStr: " + getTotalStrength() + "\nDex: " + getTotalDexterity() +
                "\nInt: " + getTotalIntelligence() + "\nLvl: " + getCurrentLevel() + "\nXP to next: " + xpToNextLevel();
        return res;
    }
}
