package com.noroff.java.hero;

import com.noroff.java.Level;

public class Warrior implements HeroAttributeStrategy{

    @Override
    public void getBaseStats(HeroAttribute stats){
        stats.health = 150;
        stats.strength = 10;
        stats.dexterity = 3;
        stats.intelligence = 1;
        //xp
        //level
    }

    @Override
    public void onLevelUp(HeroAttribute stats){
        stats.health += 30;
        stats.strength += 5;
        stats.dexterity += 2;
        stats.intelligence += 1;
    }

}
