package com.noroff.java.hero;

public enum HeroType {
    MAGE,
    RANGER,
    WARRIOR
}
