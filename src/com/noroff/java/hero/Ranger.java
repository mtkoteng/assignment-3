package com.noroff.java.hero;

import com.noroff.java.Level;

public class Ranger implements HeroAttributeStrategy{

    @Override
    public void getBaseStats(HeroAttribute stats){
        stats.health = 120;
        stats.strength = 5;
        stats.dexterity = 10;
        stats.intelligence = 2;
    }


    @Override
    public void onLevelUp(HeroAttribute stats){
        stats.health += 20;
        stats.strength += 2;
        stats.dexterity += 5;
        stats.intelligence += 1;
    }



}
