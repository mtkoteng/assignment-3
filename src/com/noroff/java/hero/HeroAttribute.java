package com.noroff.java.hero;

//Class for setting the attributes to a Hero
public class HeroAttribute {
    public int health;
    public int strength;
    public int dexterity;
    public int intelligence;
}
