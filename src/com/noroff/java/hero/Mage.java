package com.noroff.java.hero;

import com.noroff.java.Level;

public class Mage implements HeroAttributeStrategy {

    @Override
    public void getBaseStats(HeroAttribute stats){
        stats.health = 100;
        stats.strength = 2;
        stats.dexterity = 3;
        stats.intelligence = 10;
    }


    @Override
    public void onLevelUp(HeroAttribute stats){
        stats.health += 15;
        stats.strength += 1;
        stats.dexterity += 2;
        stats.intelligence += 5;
    }


}
