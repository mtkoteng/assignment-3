package com.noroff.java.hero;

//Interface for handling the calculations for the heroes
public interface HeroAttributeStrategy {
    void getBaseStats(HeroAttribute stats);
    void onLevelUp(HeroAttribute stats);
}
