package com.noroff.java.item;

public enum Slot {
    HEAD,
    BODY,
    LEGS,
    WEAPON
}
