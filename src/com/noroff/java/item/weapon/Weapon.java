package com.noroff.java.item.weapon;

import com.noroff.java.item.Item;
import com.noroff.java.item.Slot;

/*
* The class is setting an attribute specific for weapons, and are inheriting the attributes
* from Items.
*/
public abstract class Weapon extends Item {
    protected int baseDamage;

    public Weapon(int neededLevel, String name){
        super(neededLevel, name, Slot.WEAPON);
    }

    public int getBaseDamage(){
        return baseDamage;
    }


    public String printInfo(){
        String res = "Item stats for: " + getName() + "\nWeapon type: ";
        if (this instanceof Melee){
            res += "Melee";
        }
        if (this instanceof Magic){
            res += "Magic";
        }
        if (this instanceof Ranged){
            res += "Ranged";
        }
        res += "\nWeapon Level: " + getNeededLevel() + "\nDamage: " + getBaseDamage();
        return res;
    }
}
