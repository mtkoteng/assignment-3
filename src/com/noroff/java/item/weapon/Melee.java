package com.noroff.java.item.weapon;

import com.noroff.java.hero.Hero;

public class Melee extends Weapon{
    public Melee(int neededLevel, String name){
        super(neededLevel, name);
        updateStats();
    }



    @Override
    public void updateStats() {
        baseDamage = 15 + (getNeededLevel() * 2);
    }
}
