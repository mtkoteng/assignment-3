package com.noroff.java.item.weapon;

import com.noroff.java.hero.Hero;

public class Magic extends Weapon{
    public Magic(int neededLevel, String name){
        super(neededLevel, name);
        updateStats();
    }

    @Override
    public void updateStats() {
        baseDamage = 25 + (getNeededLevel() * 2);
    }
}
