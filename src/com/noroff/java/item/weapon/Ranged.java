package com.noroff.java.item.weapon;

import com.noroff.java.hero.Hero;

public class Ranged extends Weapon{
    public Ranged(int neededLevel, String name){
        super(neededLevel, name);
        updateStats();
    }

    @Override
    public void updateStats() {
        baseDamage = 5 + (getNeededLevel() * 3);
    }
}
