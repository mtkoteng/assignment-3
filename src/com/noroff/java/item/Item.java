package com.noroff.java.item;

/*
* An abstract class creating the common attributes for all Items, and an abstract method
* for updating the stats of an Item.
*/
public abstract class Item {
    private int neededLevel;
    private String name;
    protected Slot slot;

    public Item(int neededLevel, String name, Slot slot){
        this.neededLevel = neededLevel;
        this.name = name;
        this.slot = slot;
    }

    public Slot getSlot() {
        return slot;
    }

    public int getNeededLevel() {
        return neededLevel;
    }

    public String getName() {
        return name;
    }

    public abstract void updateStats();

}
