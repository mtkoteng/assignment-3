package com.noroff.java.item.armor;

import com.noroff.java.hero.Hero;
import com.noroff.java.item.Slot;

public class Leather extends Armor{

    public Leather(int neededLevel, String name, Slot slot){
        super(neededLevel, name, slot);
        updateStats();
    }

    @Override
    public void updateStats() {
        bonusHp = 20 + (8 * getNeededLevel());
        bonusDex = 3 + (2 * getNeededLevel());
        bonusInt = 1 + getNeededLevel();
        scale();
    }

    public String printInfo(){
        String info = "Item stats for: " + getName() + "\nArmor type: " + setArmorType()
                + "\nSlot: " + slot + "\nArmor level: " + getNeededLevel() + "\nBonus hp: "
                + bonusHp + "\nBonus dex: " + bonusDex + "\nBonus int: " + bonusInt;
        return info;
    }
}
