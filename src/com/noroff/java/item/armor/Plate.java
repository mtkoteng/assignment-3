package com.noroff.java.item.armor;

import com.noroff.java.hero.Hero;
import com.noroff.java.item.Slot;

public class Plate extends Armor{

    public Plate(int neededLevel, String name, Slot slot){
        super(neededLevel, name, slot);
        updateStats();
    }

    @Override
    public void updateStats() {
        bonusHp = 30 + (12 * getNeededLevel());
        bonusStr = 3 + (2 * getNeededLevel());
        bonusDex = 1 + getNeededLevel();
        scale();
    }

    public String printInfo(){
        String info = "Item stats for: " + getName() + "\nArmor type: " + setArmorType()
                + "\nSlot: " + slot + "\nArmor level: " + getNeededLevel() + "\nBonus hp: "
                + bonusHp + "\nBonus dex: " + bonusDex + "\nBonus str: " + bonusStr;
        return info;
    }
}
