package com.noroff.java.item.armor;

import com.noroff.java.hero.Hero;
import com.noroff.java.item.Item;
import com.noroff.java.item.Slot;

import java.util.ArrayList;

/*
* This class calculates the armor stats based on the type of armor.
*/
public abstract class Armor extends Item {
    protected int bonusHp;
    protected int bonusStr;
    protected int bonusDex;
    protected int bonusInt;

    public Armor(int neededLevel, String name, Slot slot){
        super(neededLevel, name, slot);
    }

    public int getBonusHp() {
        return bonusHp;
    }

    public int getBonusStr() {
        return bonusStr;
    }

    public int getBonusDex() {
        return bonusDex;
    }

    public int getBonusInt() {
        return bonusInt;
    }

    //Scales the bonuses based on what slot it is in
    protected void scale(){
        double scaling = 1.0;
        if (slot == Slot.BODY){
            return;
        }
        if (slot == Slot.HEAD){
            scaling = 0.8;
        }
        if (slot == Slot.LEGS){
            scaling = 0.6;
        }
        bonusHp = (int) (bonusHp * scaling);
        bonusStr = (int) (bonusStr * scaling);
        bonusDex = (int) (bonusDex * scaling);
        bonusInt = (int) (bonusInt * scaling);
    }

    public String setArmorType(){
        String res = "";
        if (this instanceof Cloth){
            res = "Cloth";
        }
        if (this instanceof Leather){
            res = "Leather";
        }
        if (this instanceof Plate){
            res = "Plate";
        }
        return res;
    }

}
