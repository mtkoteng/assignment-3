package com.noroff.java;

/*
* This class is handling the levels for the heroes
* It creates an array with 20 levels, calculates the amount of xp needed for next level
* and finds the current level. It also calculates how many xp is needed to next level.
*/
public class Level {
    private int[] limits = new int[20];   //20 levels

    public Level(){}

    public int getCurrentLevel(int xp){
        limits = getLevelLimits();
        int currentLevel = 0;
        if (xp < 100){
            currentLevel = 1;
        }
        for (int i = 1; i < limits.length; i++) {
            if (xp >= limits[i-1] && xp < limits[i]){
                currentLevel = i+1;
            }
        }
        return currentLevel;
    }

    //Calculates how many xp is needed for each level
    public int[] getLevelLimits(){
        double increaseFactor = 1.10;   //10%
        int startLimit = 100;
        limits[0] = startLimit;
        for (int i = 1; i < 20; i++) {
            limits[i] = (int) (startLimit * increaseFactor);
            startLimit = limits[i];
        }
        return limits;
    }

    public int xpToNextLevel(int xp){
        int currentLevel = getCurrentLevel(xp);
        int toNext = limits[currentLevel-1] - xp;
        return toNext;
    }
}
